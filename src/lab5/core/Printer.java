package lab5.core;

/**
 * The terrible 3rd party class that is not an interface but that we really
 * want to mock.
 */
public abstract class Printer {
    public abstract void print(String toPrint);
}