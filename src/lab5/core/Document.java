package lab5.core;

/**
 * Our nice class that is allowed to print
 */
public class Document {

    private final Printer printer;

    private String content;

    public Document(final Printer printer) {
        this.printer = printer;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public void print() {
        printer.print(content);
    }
}

