package library.fees;



public class LateFeesCruel implements LateFeesStrategy {

	@Override
	public boolean isLate(int days) {
		return days>14;
	}

	@Override
	public double lateFee(int days) {
		return (days-14)*5.00;
	}

}
