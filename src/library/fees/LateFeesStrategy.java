package library.fees;

public interface LateFeesStrategy {
	public boolean isLate(int days);
	public double lateFee(int days);
}
