package library.test;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.LocalDate;

import library.core.Book;
import library.core.Member;
import library.core.Publication;
import library.fees.LateFeesCruel;
import library.fees.LateFeesSimple;

import org.junit.Before;
import org.junit.Test;

public class TestPublication {
	
	Publication pub;
	Member max;
	
	@Before
	public void setUp() {
		pub = new Book("Lords of the Rings", new LateFeesSimple());
		max = new Member("Max");
	}
	
	@Test
	public void testCheckout() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		pub.checkout(max,checkoutDate);
		assertTrue(pub.isCheckout());
		assertEquals(max, pub.getMember());
		assertEquals(checkoutDate, pub.getCheckoutDate());
	}
	
	@Test
	public void testReturnOk() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 3);
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(0.00, max.getDueFees(),0.01);
	}
	
	@Test
	public void testReturnLateSimple() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(5.00, max.getDueFees(),0.01);
	}
	
	@Test
	public void testReturnLateCruel() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		pub.setLateFeeStrategy(new LateFeesCruel());
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(25.00, max.getDueFees(),0.01);
	}
	
}
