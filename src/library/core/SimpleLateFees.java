package library.core;


public class SimpleLateFees implements LateFeesStrategy {

	@Override
	public boolean isLate(int days) {
		return days>14;
	}

	@Override
	public double lateFee(int days) {
		return (days-14)*1.00;
	}

}
