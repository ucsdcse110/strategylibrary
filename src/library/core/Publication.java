package library.core;

import java.time.LocalDate;
import java.time.Period;

import library.fees.LateFeesStrategy;

public abstract class Publication {

	private Member hasBook;
	private LocalDate checkoutDate;
	private LateFeesStrategy lateFees;
	
	public Publication(LateFeesStrategy lateFees) {
		this.lateFees = lateFees;
	}
	
	public void pubReturn(LocalDate returnDate) {
		int daysKept = Period.between(checkoutDate, returnDate).getDays();
		if (lateFees.isLate(daysKept))
			hasBook.applyLateFee(lateFees.lateFee(daysKept));
		hasBook=null;
		checkoutDate=null;
	}
	
	public void checkout(Member member, LocalDate checkoutDate2) {
		hasBook=member;
		this.checkoutDate = checkoutDate2;
	}

	public boolean isCheckout() {
		return checkoutDate!=null;
	}

	public Member getMember() {
		return hasBook;
	}

	public LocalDate getCheckoutDate() {
		return checkoutDate;
	}
	
	public void setLateFeeStrategy(LateFeesStrategy lateFees) {
		this.lateFees = lateFees;
	}
	
}
