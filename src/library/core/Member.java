package library.core;

public class Member {
	private String name;
	private double fees; 
	public Member(String string) {
		name=string;
	}

	public double getDueFees() {
		return fees;
	}

	public void applyLateFee(double i) {
		fees+=i;		
	}

}
